import express from 'express';
import mongoose from "mongoose";
import cors  from "cors";

const app = express();
// The app.use() function adds a new middleware to the app
// express.json() is a built in middleware function in Express starting from v4.16.0. It parses incoming JSON requests and puts the parsed data in req.body.
app.use(express.json());

// Mongoose makes returning updated documents or query results easier
mongoose.connect('mongodb://127.0.0.1:27017/expenseTracker')

// CORS or Cross-Origin Resource Sharing in Node. js is a mechanism by which a front-end client can make requests for resources to an external back-end server
app.use(cors())

const PORT = process.env.PORT || 5500;
app.listen(PORT, () => {
  console.log("Server Connected!!");
})